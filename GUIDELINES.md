## Modules

Each module should following folder structure:
(1) Config folder (config and interface file)
(2) Services folder (all module services)
(3) Features component(s)

### Module config
Module config consists of at least two files:
(1) Config file named as moduleName-module.config.ts
(2) Interface (data models) file named moduleName-module.ts

Config file should export initial state of module and all other config or state configuration.

Interface file should export all interfaces that are used in module. In case interface is shared by more then one module then it should be moved to core module.

### Module services
Module services consist of at least three different services:
(1) Module state service named moduleName-module-state.service.ts
(2) Module action service named moduleName-module-action.service.ts
(3) Module API service named moduleName-module-api.service.ts
(4) Other services needed by the module (e.g. wizard service)

Module state service holds the current state of module. If neccessary it also subscribes to the global state and filters those properties that are needed by the module (usually at least apiURL). State for module components is provided by state observable stream.

Module action service should handle actions that were dispatched by components (like clicks, scroll events, etc.). It either dispatches new state, dispatches to API service method or any other endpoint (other services). In other words it is combination of action and reducer function(s) that are used by ngrx or Redux.

Module API service should handle all request, websocket or server-side events streams. It is the only interface against remote API server.

### Module feature components
Module should have one container component and various feature components that placed in either primary or secondary router outlets. Component interaction should be done via service. All components must have OnPush change detection enabled. Interaction via @Input and @Output should be used only with low level components (e.g. inputs, material components).



