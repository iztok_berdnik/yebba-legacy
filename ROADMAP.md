# Yebba Web Client roadmap
Roadmap could change and this file should reflect that

## Version 0 - No name

### Release 0.0.1
Application features:
(1) Empty screen with application specific loading spinner (#8)
(2) Implemented environment variables (#5)

Other features:
(1) Defined Changelog file (#7)
(2) Defined license file (#3)
(3) Defined Guidelines for development (#2)
(4) Accurate Readme file (#1)

### Release 0.0.2
Application features:
(1) Storage service (#9)
(2) Geolocation service (#10)
(3) Client environment service (#11)
(4) Application settings service (#12)
(5) Localization service (#18)

### Release 0.0.3
Application features:
(1) App module state service (#13)
(2) App module action service (#14)
(3) App module init (#15)
(4) Application themes and styling (#16)
(5) Not found module (#17)

### Release 0.0.4
Application features:
(1) Authentication module state service (#19)
(2) Authentication module action service (#20)
(3) Authentication module api service (#21)
(4) Authentication module container component and module routing (#22)

### Release 0.0.5
Application feature:
(1) Application shell (container) module

## Version 1 - Aurora Austin
