# Release 0.0.1 #
## Features in this release ##
[FEAT] Design guidelines and principles. Closes #2

[FEAT] Changelog initial commit. Closes #7

[FEAT] Update readme to be in line with actual app instead of generic cli generated content. Closes #1

[FEAT] Roadmap initial commit. Closes #6

[FEAT] Change of tsconfig to allow @app and @Env By Musaid alias. Closes #4

[FEAT] Basic environment variables. Closes #5

[FEAT] License file with basic license information. Closes #3

[FEAT] Loading boot spinner. Closes #8

## Breaking changes ##

## Bugfix ##

## Other changes in this release ##
[Chore] Updated ROADMAP.md with issue tracking numbers. Added new features in various releases.

[Chore] Change app-root to yb-root to be compliant with component selector

[Chore] Changed components prefix from app to yb.
