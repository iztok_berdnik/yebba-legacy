/**
 * @license
 * Copyright (c) yebba. All rights reserved.
 */

import { IApplicationEnvironment } from '@app/core/config/app-module';


// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: IApplicationEnvironment = {
  build: 'DEVELOPMENT',
  production: false,
  developmentMode: true,
  apiURL: 'http://localhost:3005'
};
