/**
 * @license
 * Copyright (c) yebba. All rights reserved.
 */

import { IApplicationEnvironment } from '@app/core/config/app-module';

export const environment: IApplicationEnvironment = {
  build: 'PRODUCTION',
  production: true,
  developmentMode: false,
  apiURL: ''
};

