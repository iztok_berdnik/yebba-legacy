/**
 * @license
 * Copyright (c) yebba. All rights reserved.
 */

 /**
  * @description
  * Application environment interface as defined in environment.ts and environment.prod.ts files.
  */
export interface IApplicationEnvironment {
  build: string;
  production: boolean;
  developmentMode: boolean;
  apiURL: string;
}
